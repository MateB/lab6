//Mate Barabas 1834578
package lab6;
import java.util.*;
public class RpsGame 
{
	int wins = 0;
	int ties = 0;
	int losses = 0;
	
	Random rand = new Random();
	
	public int getWins()
	{
		return this.wins;
	}
	
	public int getTies()
	{
		return this.ties;
	}
	
	public int getLosses()
	{
		return this.losses;
	}
	
	public String playRound(String input)
	{
		int comp = rand.nextInt(3);
		System.out.println(comp);
		String r = "rock"; //rock for computer is 0
		String p = "paper";//paper for computer is 1
		//scissors for computer is 2
		
		if (comp == 0)
		{
			if(input.equals(r))
			{
				this.ties++;
				return ("Computer chose rock, you chose: " + input + " its a tie!");
				
			}
			else if(input.equals(p))
			{
				this.wins++;
				return ("Computer chose rock, you chose: " + input + " you win!");
			}
			else
			{
				this.losses++;
				return ("Computer chose rock, you chose: " + input + " you lost:(");
			}
		}
		else if (comp == 1)
		{
			if(input.equals(r))
			{
				this.losses++;				
				return ("Computer chose paper, you chose: " + input + " you lost:(");
			}
			else if(input.equals(p))
			{
				this.ties++;
				return ("Computer chose paper, you chose: " + input + " you tied!");
			}
			else
			{
				this.wins++;
				return ("Computer chose paper, you chose: " + input + " win!");
			}
		}
		else
		{
			if(input.equals(r))
			{
				this.wins++;			
				return ("Computer chose scissors, you chose: " + input + " win!");
			}
			else if(input.equals(p))
			{
				this.losses++;
				return ("Computer chose scissors, you chose: " + input + " you lost:(");
			}
			else
			{
				this.ties++;
				return ("Computer chose scissors, you chose: " + input + " you tied!");
			}
		}
	}
}
