//Mate Barabas 1834578
package lab6;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	
	public void start(Stage stage) {
		Group root = new Group();		
		HBox buttons = new HBox();
		HBox textFields = new HBox();
		VBox vbox = new VBox();
		RpsGame rps = new RpsGame();
			
		Button rButton = new Button("Rock");
		Button pButton = new Button("Paper");
		Button sButton = new Button("Scissors");
		
		TextField welcomeText = new TextField("Welcome!");
		TextField winsText = new TextField("Wins: " + rps.getWins());
		TextField lossesText = new TextField("Losses: " + rps.getLosses());
		TextField tiesText = new TextField("Ties: " + rps.getTies());
		
		RpsChoice actionObject1 = new RpsChoice("rock", welcomeText, winsText, lossesText, tiesText, rps);
		rButton.setOnAction(actionObject1);

		RpsChoice actionObject2 = new RpsChoice("paper", welcomeText, winsText, lossesText, tiesText, rps);
		pButton.setOnAction(actionObject2);
		
		RpsChoice actionObject3 = new RpsChoice("scissors", welcomeText, winsText, lossesText, tiesText, rps);
		sButton.setOnAction(actionObject3);

		welcomeText.setPrefWidth(400);
		winsText.setPrefWidth(200);
		lossesText.setPrefWidth(200);
		tiesText.setPrefWidth(200);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
	
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene);
		buttons.getChildren().addAll(rButton, pButton, sButton);
		textFields.getChildren().addAll(welcomeText, winsText, lossesText, tiesText);
		vbox.getChildren().addAll(buttons, textFields);
		root.getChildren().add(vbox);
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
