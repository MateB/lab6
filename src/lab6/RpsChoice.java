//Mate Barabas 1834578
package lab6;
import javafx.event.EventHandler;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent> {
    private String input;
    private TextField msg;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private RpsGame rps = new RpsGame();
    
    public RpsChoice(String userInput, TextField msg, TextField wins, TextField losses, TextField ties, RpsGame rps) {
        this.input = userInput;
        this.msg = msg;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.rps = rps;
    }
    
    @Override
    public void handle(ActionEvent e) {
        String msgTemp = rps.playRound(input);
        msg.setText(msgTemp);
        int winsTemp = rps.getWins();
        wins.setText(String.valueOf(winsTemp));
        int lossTemp = rps.getLosses();
        losses.setText(String.valueOf(lossTemp));
        int tiesTemp = rps.getTies();
        ties.setText(String.valueOf(tiesTemp));
    } 
}
